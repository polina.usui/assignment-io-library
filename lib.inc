section .text

exit:
mov rax, 60
syscall


string_length:
    mov rax, -1 ; string length
 .loop:
  inc rax
  cmp byte[rdi+rax], 0
  jnz .loop
    ret


print_string:
 call string_length
 mov rsi, rdi ; pointer to string
 mov rdx, rax ; string length
 mov rax, 1 ; sys_write
 mov rdi, 1 ; stdout
 syscall
    ret


print_newline:
 mov rdi, 10;

print_char: ;
    mov rax, 1 ; sys_write
 push rdi ; pushing rdi(char) to memory for pointer
 mov rsi, rsp ; pointer to char
 mov rdi, 1 ; stdout
 mov rdx, 1 ; length=1
 syscall
 pop rdi ; to fix rsp
    ret



print_int:
 cmp rdi, 0 ; check if < 0
 jns print_uint
    xor rdi, -1 ; make uint
 inc rdi ;
 push rdi
 mov rdi, '-' ; print '-'
 call print_char ;
 pop rdi


print_uint:
 mov rax, rdi
    mov r8, 10 ; 10for div
 times 1 push word 0 ; 
 mov r9, rsp ; buffer for rsp
 add rsp, 1 ; to last stack mem cell
 mov byte [rsp], 0 ; end of the string
 .loop:
  xor rdx, rdx ; RDX:RAX when div
  dec rsp
  div r8
  add rdx, '0' ; RDX has remainder
  mov byte [rsp], dl ; byte of RDX
  test rax, rax
  jnz .loop
 mov rdi, rsp ; pointer to string
 call print_string
 mov rsp, r9
 add rsp, 2 ; fix rsp
    ret


string_equals:
 call string_length
 mov r8, rax ; r8 - length of 1 string
 mov rax, rdi
 mov rdi, rsi
 mov rsi, rax
 
 call string_length
 
 mov rcx, rax ; rcx - length of 2 string
 xor rax, rax
 cmp r8, rcx ; if r8!=rcx |=> exit
 jnz .exit
 test rcx, rcx ; if r8=rcx=0 |=> return 1
 jz .nullstr
 .loop:
  dec rcx
  mov r9b, byte[rdi+rcx]
  cmp r9b, byte[rsi+rcx]
  jnz .exit ; if != |=> return 0
  test rcx, rcx
  jnz .loop
 .nullstr:
 inc rax
 .exit:
    ret


read_char:
    xor rax, rax
 push rax
 mov rdi, 0 ; sys_read
 mov rsi, rsp ; pointer
 mov rdx, 1 ; size
 syscall
 mov rax, [rsp]
 add rsp, 8 ; fix rsp
    ret


read_word:
   .space:   ; skip until first letter
 push rdi
 push rsi
 push rdx 
 call read_char
 pop rdx
 pop rsi
 pop rdi 

 cmp rax, 0x09
 je .space
 cmp rax, 0x0A
 je .space
 cmp rax, 0x20
 je .space
 xor rdx, rdx 
  
  .next_char:
 cmp rdx, rsi
 je .error
 
 cmp rax, 0x00  
 je .success
 cmp rax, 0x09
 je .success
 cmp rax, 0x0A
 je .success
 cmp rax, 0x20
 je .success

 mov byte [rdi + rdx], al
 inc rdx
 
 push rdi
 push rsi
 push rdx
 call read_char
 pop rdx
 pop rsi
 pop rdi 

 jmp .next_char

  .success:
 mov byte [rdi + rdx], 0  
 mov rax, rdi   
 jmp .end
  .error:
 xor rax, rax   
  .end: 
  ret


; rdi points to a string
; returns rax: number, rdx : length
parse_uint:
 xor rdx, rdx ; RDX:RAX
 xor rax, rax ; RDX:RAX
 xor rcx, rcx ; Counter
 xor r11, r11 ; Buffer
 mov r8, '0'
 mov r9, '9'
 mov r10, 10 ; for mul
 .loop:
  mov r11b, byte[rdi+rcx]
  cmp r11b, r8b ; (char)-'0'
  js .break
  cmp r9b, r11b ; '9'-(char)
  js .break
  sub r11, r8 ; char to uint
  mul r10 ;
  add rax, r11 ;
  inc rcx ; length+1
  test rax, rax ; if zero |=> break
  jnz .loop
 .break:
 mov rdx, rcx
    ret



; rdi points to a string
; returns rax: number, rdx : length
parse_int:
 push r12
 xor r12, r12 ; buffer for '-' or...
 cmp byte[rdi], '-'
 jnz .plus ; if not '-' go parse_uint
 inc rdi ; rdi+1 for uint
 mov r12, -1
 .plus:
 call parse_uint ; same as uint if plus
 xor rax, r12
 sub rax, r12
 sub rdx, r12
 pop r12
 ret


string_copy:
 .loop:
  xor rax, rax
  mov al, byte[rdi]
  mov byte[rsi], al
  inc rdi
  inc rsi
  test rax, rax
  jnz .loop
 ret
